let
  pkgs = import <nixpkgs> {  };
in
  pkgs.haskellPackages.callPackage ./pdb-parser.nix { }
