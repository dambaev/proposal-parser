{--| This module describes specifications for a given parser
 -}
module Main
  ( main
  ) where

import Test.Hspec

import Data.PDB as PDB
import Data.PDB.Parser as PDBParser
import Text.Parsec as Parsec
import Data.Text as Text

main :: IO ()
main = hspec $ do
  floatsParserSpecs
  atomSpecs
  terSpecs
  residueNameSpecs
  chainIDSpecs
  describe "proposal test" $ do
    it "contextless parser from proposal" $ do
      PDBParser.parse " ATOM C CA 0.111 0.456 1.345 ATOM N N1 0.356 0.789 10.458 TER OXT O "
        `shouldBe` (Right
        [ ATOM (ChainID 'C') (ResName "CA") (Coordinates 0.111 0.456 1.345)
        , ATOM (ChainID 'N') (ResName "N1") (Coordinates 0.356 0.789 10.458)
        , TER (ResName "OXT") (ChainID 'O')
        ])

-- simple combinator, that acts like endBy but gathers only 1 element instead of list
singleEndBy :: Parsec Text m a-> Parsec Text m ()-> Parsec Text m a
singleEndBy p delim = p >>= \ret-> delim >> return ret

chainIDSpecs :: SpecWith ()
chainIDSpecs = describe "chain ID parser" $
  it "correct chain ID" $ Parsec.parse (PDBParser.chainID `singleEndBy` eof) "" "N"
    `shouldBe` (Right (PDB.ChainID 'N'))

residueNameSpecs :: SpecWith ()
residueNameSpecs = describe "ResidueName parser" $
  it "correct residue name" $
    Parsec.parse (PDBParser.residueName `singleEndBy` eof) "" "N1"
      `shouldBe` (Right (PDB.ResName "N1"))

terSpecs :: SpecWith ()
terSpecs = describe "TER parser" $ do
  it "TER OXT O" $
    Parsec.parse (PDBParser.ter `singleEndBy` eof) "" "TER OXT O"
      `shouldBe` (Right $ PDB.TER (ResName "OXT") (ChainID 'O'))

atomSpecs :: SpecWith ()
atomSpecs = describe "ATOM parser" $ do
  it "contextless parser of atom" $ do
    Parsec.parse (PDBParser.atom `singleEndBy` eof) "" "ATOM C CA 0.1 1.0 2.1"
      `shouldBe`
      (Right $ ATOM (ChainID 'C') (ResName "CA") (Coordinates 0.1 1.0 2.1))

floatsParserSpecs :: SpecWith ()
floatsParserSpecs = describe "floats parser tests" $ do
  mapM_ performTest tests
  where
   performTest (descr, input, predicate, result) = it descr $
     Parsec.parse (PDBParser.float `singleEndBy` eof) "" input
       `predicate` result
   tests = [ ("alpha in float", "a1.1", shouldNotBe, Right 1.1)
           , ("two dots in float1", "1..1", shouldNotBe, Right 1.1)
           , ("two dots in float2", "1.1.", shouldNotBe, Right 1.1)
           , ("empty string", "", shouldNotBe, Right 0.0)
           , ("0.1234", "0.1234", shouldBe, Right 0.1234)
           , ("12345678.12345678", "12345678.12345678", shouldBe, Right 12345678.12345678)
           ]
