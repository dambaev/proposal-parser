# Proposal parser

The task of proposal is to parse and extract data from given string. But there is no context on which data should be extracted. Based on given string,
I have made a guess, that this data's format is some subset of PDB format (but I may be wrong, of course).
So this project consists from:
1. `Data.PDB` module, that contains data structures;
2. `Data.PDB.Parser` module, that contains Parsec parsers;
2. `./test/spec.hs` module, that contains specifications for `Data.PDB.Parser`.

As there is not much details about format of this specific string from test, I haven't spent too much time on guessing specifications for it.
As there is no other task than parsing, executable is not doing anything meaningful.

See `./test/spec.hs` for reference

# Building

First of all, you will need to clone repo:

```
git clone https://gitlab.com/dambaev/proposal-parser.git
```

The rest depends on your OS

## NixOS

```
$ nix-shell --pure ./shell.nix
nix-shell $ cabal test
```

## Other OSes

```
cabal test
```


