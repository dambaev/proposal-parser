{--| This module provides number of parsers
 -}
module Data.PDB.Parser
  ( parse
  , parseRecords
  , record
  , float
  , atom
  , ter
  , residueName
  , chainID
  ) where

import qualified Data.PDB as PDB
import Data.Text as Text
import qualified Text.Parsec.Prim as Parsec
import qualified Text.Parsec.Combinator as Parsec
import qualified Text.Parsec.Text as Parsec
import qualified Text.Parsec as Parsec
import Text.Parsec hiding ( parse )
import Data.List as List
import Data.Char as Char

parse :: Text-> Either ParseError [PDB.Record]
parse input = Parsec.parse parseRecords "" input

parseRecords :: Parsec Text m [PDB.Record]
parseRecords = do
  spaces
  endBy record ((space >> return ()) <|> eof)

record :: Parsec Text m PDB.Record
record = try atom <|> try ter

float :: Parsec Text m Float
float = do
  ret <- many1 $ satisfy (\x-> Char.isDigit x || x == '.') -- either digit or dot
  case () of
    _ | List.length (List.filter (=='.') ret) /= 1 ->
        parserFail "expected exactly 1 dot"
    _ | List.length ret < 3 -> parserFail "not enough chars for float"
    _ -> return $ read ret

atom :: Parsec Text m PDB.Record
atom = do
  string "ATOM"
  many1 space
  chainId <- chainID
  many1 space
  name <- residueName
  many1 space
  (x,y,z) <- coordinates
  return $ PDB.ATOM chainId name (PDB.Coordinates x y z)
  where
    coordinates = do
      x <- float
      many1 space
      y <- float
      many1 space
      z <- float
      return (x, y, z)

chainID :: Parsec Text m PDB.ChainID
chainID = upper >>= return . PDB.ChainID

residueName :: Parsec Text m PDB.ResName
residueName = do
  name <- many1 $ satisfy (\c-> isDigit c || isUpper c)
  case () of
    _ | List.length (List.filter isDigit name) == List.length name->
        parserFail "residue name should start with at least 1 upper case letter"
    _ -> return (PDB.ResName $ Text.pack name)

ter :: Parsec Text m PDB.Record
ter = do
  string "TER"
  many1 space
  name <- residueName
  many1 space
  chainId <- chainID
  return $ PDB.TER name chainId
