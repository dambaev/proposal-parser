{--| This module describes a subset of data structures (possibly, from Atomic
 - Coordinate Entry Format) in a form, that is enough to pass test spec.
 -}
module Data.PDB
  ( Record(..)
  , Coordinates(..)
  , ResName (..)
  , ChainID (..)
  ) where

import Data.Text as Text
import Data.Text.Encoding as Text

data Coordinates
  = Coordinates
    { x :: Float
    , y :: Float
    , z :: Float
    }
  deriving (Show, Eq)

newtype ResName = ResName Text
  deriving (Show, Eq)

newtype ChainID = ChainID Char
  deriving (Show, Eq)

data Record
  = ATOM
    { chainID   :: ChainID
    , resName   :: ResName
    , coords    :: Coordinates
    }
  | TER
    { resName :: ResName
    , chainID :: ChainID
    }
  deriving (Show, Eq)
