{ mkDerivation, base, hspec, parsec, stdenv, text }:
mkDerivation {
  pname = "pdb-parser";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [ base parsec text ];
  executableHaskellDepends = [ base ];
  testHaskellDepends = [ base hspec parsec text ];
  description = "PDB parser";
  license = stdenv.lib.licenses.bsd3;
}
